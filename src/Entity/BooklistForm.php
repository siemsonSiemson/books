<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 06.12.18
 * Time: 15:38
 */

namespace App\Entity;


class BooklistForm
{

    public $name;
    public $titles;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        return $this->name = $name;
    }

    public function getTitles()
    {
        return $this->titles;
    }

    public function setTitles($titles)
    {
        return $this->titles = $titles;
    }

}