<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BooklistToBookRepository")
 */
class BooklistToBook
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $booklistId;

    /**
     * @ORM\Column(type="integer")
     */
    private $bookId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBooklistId(): ?int
    {
        return $this->booklistId;
    }

    public function setBooklistId(int $booklistId): self
    {
        $this->booklistId = $booklistId;

        return $this;
    }

    public function getBookId(): ?int
    {
        return $this->bookId;
    }

    public function setBookId(int $bookId): self
    {
        $this->bookId = $bookId;

        return $this;
    }
}
