<?php

namespace App\Repository;

use App\Entity\BooklistToBook;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BooklistToBook|null find($id, $lockMode = null, $lockVersion = null)
 * @method BooklistToBook|null findOneBy(array $criteria, array $orderBy = null)
 * @method BooklistToBook[]    findAll()
 * @method BooklistToBook[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BooklistToBookRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BooklistToBook::class);
    }

    // /**
    //  * @return BooklistToBook[] Returns an array of BooklistToBook objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BooklistToBook
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
