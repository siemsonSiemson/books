<?php

namespace App\Repository;

use App\Entity\Booklist;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Booklist|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booklist|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booklist[]    findAll()
 * @method Booklist[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BooklistRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Booklist::class);
    }

    // /**
    //  * @return Booklist[] Returns an array of Booklist objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function findByName($value): ?Booklist
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.name = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /*public function findBooklistWithTitles($id): ?Booklist
    {
        $qb = $this->createQueryBuilder('b');
        $qb->select('')
            ->from('App\Entity\Booklist', 'b1')
            ->leftJoin('App\Entity\BooklistToBook',
                'b2',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'b1.id = b2.booklistId');

        var_dump($qb->getQuery()->getResult());
        die;

        return $qb->getQuery()->getResult();
    }*/

}
