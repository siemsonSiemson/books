<?php

namespace App\Controller;

use App\Entity\Mates;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\LoginForm;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function index(Request $request)
    {

        $session = $this->container->get('session');

        if (isset($session) && $session->get('name') ) {

            return $this->redirectToRoute('book');
        }

        $loginForm = new LoginForm();
        $form = $this->createFormBuilder($loginForm)
            ->add('username', TextType::class, array('label' => 'Username:', 'attr' => array('class'=>'margin-right')))
            ->add('password', PasswordType::class, array('label' => 'Password:', 'attr' => array('class'=>'margin-right')))
            ->add('save', SubmitType::class, array('label' => 'Log on!'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $params = $request->request->get('form');
            $username = $params['username'];
            $password = $params['password'];

            $mate = $this->getDoctrine()
                ->getRepository(Mates::class)
                ->findBy(array('username' => $username, 'password' => $password), array('name' => 'ASC'));

            if (sizeof($mate) === 1 ) {

                if (!isset($session)) {
                    $session = new Session();
                    $session->start();
                    $session->set('name', ($mate[0]->getUsername()));
                }

                return $this->redirectToRoute('book');
            }
        }



        return $this->render('login/index.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
