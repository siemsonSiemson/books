<?php

namespace App\Controller;

use App\Entity\Book;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\BooklistForm;
use App\Entity\Booklist;
use App\Entity\BooklistToBook;
use Symfony\Flex\Response;

class BookController extends AbstractController
{
    /**
     * @Route("/book", name="book")
     */
    public function index()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $bookRepo = $this->getDoctrine()->getRepository(Book::class);
        $books = $bookRepo->findAll();

        return $this->render('book/index.html.twig', [
            'title' => 'Welcome at Book panel!',
            'books' => $books,
        ]);
    }

    /**
     * @Route("/addBook", name="add_book")
     */
    public function addBook(Request $request)
    {
        $book = new Book();
        $form = $this->createFormBuilder($book)
            ->add('title', TextType::class,
                array(
                    'attr' => array('class'=>'margin-right')
                )
            )
            ->add('author', TextType::class,
                array(
                    'attr' => array('class'=>'margin-right')
                )
            )
            ->add('ISBN', TextType::class,
                array(
                    'attr' => array('class'=>'margin-right')
                )
            )
            ->add('cover', TextType::class,
                array(
                    'attr' => array('class'=>'margin-right')
                )
            )
            ->add('save', SubmitType::class,
                array(
                    'label' => 'Add book!',
                    'attr' => ['class' => 'btn btn-success pull-right']
                )
            )
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($book);
            $entityManager->flush();

            return $this->redirectToRoute('book');

        } else {

            return $this->render('book/add.html.twig', array(
                'form' => $form->createView(),
            ));
        }
    }

    /**
     * @Route("/editBook", name="edit_book")
     */
    public function editBook(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Book::class);

        if (isset($_GET['id'])) {

            $id = $_GET['id'];
            $book = $repository->find($id);
            $form = $this->createFormBuilder($book)
                ->add('title', TextType::class,
                    array(
                        'attr' => array('class'=>'margin-right')
                    )
                )
                ->add('author', TextType::class,
                    array(
                        'attr' => array('class'=>'margin-right')
                    )
                )
                ->add('ISBN', TextType::class,
                    array(
                        'attr' => array('class'=>'margin-right')
                    )
                )
                ->add('cover', TextType::class,
                    array(
                        'attr' => array('class'=>'margin-right')
                    )
                )
                ->add('save', SubmitType::class,
                    array(
                        'label' => 'Update book!',
                        'attr' => ['class' => 'btn btn-success pull-right']
                    )
                )
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($book);
                $entityManager->flush();

                return $this->redirectToRoute('book');
            }

            return $this->render('book/edit.html.twig', array(
                'form' => $form->createView(),
                'id' => $id
            ));
        }
    }

    /**
     * @Route("/deleteBook", name="delete_book")
     */
    public function deleteBook(Request $request)
    {
        if (isset($_GET['id'])) {

            $id = $_GET['id'];
            $entityManager = $this->getDoctrine()->getManager();
            $repository = $this->getDoctrine()->getRepository(Book::class);
            $book = $repository->find($id);

            $entityManager->remove($book);
            $entityManager->flush();

        }

        return $this->redirectToRoute('book');
    }

    /**
     * @Route("/createbooklist", name="create_book_list")
     */
    public function createBookList(Request $request)
    {
        $bookRepository = $this->getDoctrine()->getRepository(Book::class);
        $choices = $bookRepository->getAllBookTitles();
        $choices = $bookRepository->presentTitlesMultiselect($choices);


        $bookListForm = new BooklistForm();
        $form = $this->createFormBuilder($bookListForm)
            ->add('name', TextType::class,
                array(
                    'attr' => array('class'=>'margin-right')
                )
            )
            ->add('titles', ChoiceType::class,
                array(
                    'attr' => array('class'=>'margin-right'),
                    'choices' => $choices,
                    'expanded'  => true,
                    'multiple'  => true,
                    )
            )
            ->add('save', SubmitType::class,
                array(
                    'label' => 'Create booklist!',
                'attr' => ['class' => 'btn btn-success pull-right']
                )
            )
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $name = $request->request->get('form')['name'];
            $titles = $request->request->get('form')['titles'];

            $bookList = new Booklist();
            $bookList->setName($name);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($bookList);
            $entityManager->flush();

            $bookListRepository = $this->getDoctrine()->getRepository(Booklist::class);
            $createdBooklist = $bookListRepository->findBy(array('name' => $name), array('id' => 'DESC'),1,0);
            $id = $createdBooklist[0]->getId();

            foreach ($titles as $title) {

                $booklistToBook = new BooklistToBook();
                $booklistToBook->setBooklistId($id);
                $booklistToBook->setBookId($title);
                $entityManager->persist($booklistToBook);
                $entityManager->flush();
            }

            //$newBookList = $bookListRepository->findBooklistWithTitles($id);
            return $this->json(array('Book list created - id: ' => $id));
        }

        return $this->render('book/createBooklist.html.twig', array(
            'form' => $form->createView()
        ));
    }

}
